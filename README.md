# atlassian-http
Welcome to the atlassian-http project.

Jira Monolith has forked this repo. See ticket CPLATFORM-2104 for more details"

## Releases ##

### 4.1.0

* Upgraded to SLF4J 2.0.

### 4.0.0

* ⚠️ This release did not include sources or javadoc 
* Upgraded to Java 17.
* Removed dependency on Guava as per Java Tech Stack recommendation.


### 3.x
* Use this version if you're still on Java 8.

### Tests ###
To run the tests simply execute

    mvn clean test

### Builds ###
Builds are found at [https://ecosystem-bamboo.internal.atlassian.com/browse/HTTP](https://ecosystem-bamboo.internal.atlassian.com/browse/HTTP).


## Code Quality

This repository enforces the Palantir code style using the Spotless Maven plugin. Any violations of the code style will result in a build failure.

### Guidelines:
- **Formatting Code:** Before raising a pull request, run `mvn spotless:apply` to format your code.
- **Configuring IntelliJ:** Follow [this detailed guide](https://hello.atlassian.net/wiki/spaces/AB/pages/4249202122/Spotless+configuration+in+IntelliJ+IDE) to integrate Spotless into your IntelliJ IDE.

