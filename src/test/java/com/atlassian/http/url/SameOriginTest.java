package com.atlassian.http.url;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Set;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

public class SameOriginTest {

    @Test
    public void testSameOriginWithMatchingOrigin() throws Exception {
        for (String url : Set.of("http://e.g/2", "http://e.g:123", "https://e.g", "https://e.g:1235")) {
            String urlWithPath = url + "/extra/";
            assertThatSameOriginIs(url, urlWithPath, true);
        }
    }

    @Test
    public void testSameOriginWithMatchingOriginDifferentCasing() throws Exception {
        String a = "http://example.com";
        String b = "http://EXAMPLE.com";
        assertThatSameOriginIs(a, b, true);
    }

    @Test
    public void testSameOriginWithInternationalisedDomain() throws Exception {
        /* \u00FC is the Unicode code point for the german u-umlaut, for the german word for "door", "Tuer".
        I could just put the actual unicode character—your IDE supports unicode, right? But this is probably safer.
         */
        assertThatSameOriginIs("http://t\u00FCr.com", "http://t\u00FCr.com", true);
        assertThatSameOriginIs("http://t\u00FCr.com", "http://tur.com", false);
        assertThatSameOriginIs("http://t\u00FCr.com", "http://xn--tr-xka.com", true);
        assertThatSameOriginIs("http://t\u00FCr.com:80", "http://xn--tr-xka.com", true);
        assertThatSameOriginIs("http://t\u00FCr.com:80", "http://t\u00FCr.com", true);
        assertThatSameOriginIs("http://t\u00FCr.com:80", "http://tür.com/:80", true);
        assertThatSameOriginIs("https://t\u00FCr.com", "https://tür.com/:443", true);
    }

    @Test
    public void testSameOriginWhenOneOriginIncludesInvisibleCharacters() throws Exception {
        String plainString = "http://theexample.com";
        String stringWithSoftHyphen =
                "http://the\u00ADexample.com"; // That's a soft hyphen, which is normally invisible.
        // According to the IDNA specification, these two URLs are identical (after conversion to ascii), however
        // they are not considered to have the same origin here to be defensive (for security reasons) against
        // implementations that might not treat them as identical.
        assertThatSameOriginIs(plainString, stringWithSoftHyphen, false);
    }

    @Test
    public void testSameOriginWithDifferentProtocol() throws Exception {
        String a = "http://e.g";
        String b = "https://e.g";
        assertThatSameOriginIs(a, b, false);
    }

    @Test
    public void testSameOriginWithDifferentPort() throws Exception {
        String a = "http://e.g:1";
        String b = "http://e.g:2";
        assertThatSameOriginIs(a, b, false);
    }

    @Test
    public void testSameOriginWithSamePort() throws Exception {
        String a = "http://e.g";
        String b = "http://e.g:80";
        assertThatSameOriginIs(a, b, true);
    }

    @Test
    public void testSameOriginWithDifferentHost() throws Exception {
        String a = "http://e.g";
        String b = "http://example.other";
        assertThatSameOriginIs(a, b, false);
    }

    @Test
    public void testSameOriginWithInvalidAuthority() throws Exception {
        String a = "http://e.g";
        String b = "http://example.com\\@e.g";
        Exception ex = assertThrows(Exception.class, () -> assertThatSameOriginIs(a, b, false));
        // new URL("").toURI() throws different exceptions on different JDKs
        // JDK 17 throws URISyntaxException whereas JDK 21 - MalformedURLException
        if (!(ex instanceof MalformedURLException || ex instanceof URISyntaxException)) {
            fail("Expected "
                    + MalformedURLException.class.getName()
                    + " or "
                    + URISyntaxException.class.getName()
                    + " exception\nActual exception: "
                    + ex);
        }
    }

    @Test
    public void testSameOriginWhenOriginIsChromeExtension() throws MalformedURLException, URISyntaxException {
        assertThat(
                SameOrigin.isSameOrigin(
                        new URI("chrome-extension://pkgdebooadcbggdeihcmionebjnfhjcf"),
                        new URI("chrome-extension://pkgdebooadcbggdeihcmionebjnfhjcf?foo=bar&fuz=baz")),
                is(true));

        assertThat(
                SameOrigin.isSameOrigin(
                        new URI("chrome-extension://pkgdebooadcbggdeihcmionebjnfhjcf"),
                        new URI("chrome-extension://different")),
                is(false));
    }

    @Test
    public void testSameOriginFalseForAuthoritylessUris() throws MalformedURLException, URISyntaxException {
        // Mailto URI's have no authority, and therefore should never have the same origin. This test is mostly
        // to check things don't explode.
        assertThat(
                SameOrigin.isSameOrigin(new URI("mailto:user@example.com"), new URI("mailto:user@example.com")),
                is(false));
    }

    @Test
    public void testSameOriginForSchemelessUris() throws MalformedURLException, URISyntaxException {
        // "relative" URI's have no authority, and therefore should never have the same origin. This test is mostly
        // to check things don't explode.
        assertThat(
                SameOrigin.isSameOrigin(
                        new URI("docs/guide/collections/designfaq.html#28"),
                        new URI("docs/guide/collections/designfaq.html#28")),
                is(false));
    }

    @Test
    public void testSameOriginWithNulls() throws Exception {
        assertThat(SameOrigin.isSameOrigin(null, new URI("http://example.com")), is(false));
    }

    @Test
    public void testGetPortForUrl() throws Exception {
        assertThat(SameOrigin.getPortForUrl(new URL("http://a.com:12")), is(12));
        assertThat(SameOrigin.getPortForUrl(new URL("http://a.com")), is(80));
        assertThat(SameOrigin.getPortForUrl(new URL("https://a.com:12")), is(12));
        assertThat(SameOrigin.getPortForUrl(new URL("https://a.com")), is(443));
    }

    private void assertThatSameOriginIs(String a, String b, boolean expected)
            throws MalformedURLException, URISyntaxException {
        assertThat(SameOrigin.isSameOrigin(new URL(a), new URL(b)), is(expected));
        assertThat(SameOrigin.isSameOrigin(new URL(b), new URL(a)), is(expected));

        assertThat(SameOrigin.isSameOrigin(new URI(a), new URI(b)), is(expected));
        assertThat(SameOrigin.isSameOrigin(new URI(b), new URI(a)), is(expected));
    }
}
