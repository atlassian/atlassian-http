package com.atlassian.http.mime;

import java.util.Set;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class StringUtilsTest {

    @Test
    public void testNonBlankCases() {
        for (String string : Set.of("a", " b", " c ")) {
            assertFalse(StringUtils.isBlank(string));
        }
    }

    @Test
    public void testBlankCases() {
        for (String string : Set.of("", " ", "  ", "   ")) {
            assertTrue(StringUtils.isBlank(string));
        }
    }

    @Test
    public void testNullIsConsideredBlank() {
        assertTrue(StringUtils.isBlank(null));
    }
}
