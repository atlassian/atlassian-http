package com.atlassian.http.method;

import java.util.Locale;
import java.util.Set;

/**
 * This class contains functions that can be used to obtain information
 * about http methods.
 *
 * @since 1.0.10.
 */
public class Methods {
    public static boolean isMutative(String method) {
        if (Set.of("GET", "HEAD", "OPTIONS", "TRACE").contains(method.toUpperCase(Locale.US))) {
            return false;
        }
        return true;
    }
}
