package com.atlassian.http.url;

import java.net.IDN;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;
import java.util.Objects;

/**
 * This class can be used to check if two URLs are in the same origin.
 *
 * @since 1.0.10.
 */
public class SameOrigin {
    /**
     * Check if two URIs have the same origin. URIs have the same origin if the scheme (protocol), hostname, and port
     * (if present or implied by the protocol) are equal. Opaque and relative URIs are never considered to have the same
     * origin. Hostnames containing Unicode (per IDNA, RFC 3454) are compared using their ASCII equivalents unless the
     * unicode value cannot be recovered from the ASCII, in which case the unicode value is used for comparison.
     *
     * @param uri the uri to check
     * @param origin the origin to check against.
     * @return true if the given uri is the same origin as the given origin uri, otherwise returns false.
     * @since 1.1.0.
     */
    public static boolean isSameOrigin(URI uri, URI origin) throws MalformedURLException {
        if (uri == null || origin == null) {
            return false;
        }
        return check(uri, origin);
    }

    /**
     * Checks if two URLs have the same origin, using the same logic as for {@link SameOrigin#isSameOrigin(URI, URI)}.
     *
     * @param url the url to check
     * @param origin the origin to check against.
     * @return true if the given url is the same origin as the given origin url otherwise returns false.
     */
    public static boolean isSameOrigin(URL url, URL origin) {
        if (url == null || origin == null) {
            return false;
        }

        try {
            return check(url.toURI(), origin.toURI());
        } catch (URISyntaxException e) {
            return false;
        }
    }

    private static String normaliseHost(URI uri) {
        final String host = getHost(uri);
        if (host == null) {
            return null;
        }

        final String asciiHost = IDN.toASCII(host).toLowerCase(Locale.US);
        if (!IDN.toUnicode(asciiHost).equals(host.toLowerCase(Locale.US))) {
            /*
               Laundered hostname doesn't match original, something may be dodgy, so use original for comparison to
               avoid matching potentially insecure hostnames designed to take advantage of bugs in hostname handling.
            */
            return host;
        }
        return asciiHost;
    }

    private static String getHost(URI uri) {
        String host = uri.getHost();
        return host != null ? host : getHostFromAuthority(uri);
    }

    private static String getHostFromAuthority(URI uri) {
        String authority = uri.getAuthority();
        if (authority == null) {
            return null;
        }

        int index = authority.indexOf(":");
        if (index != -1) {
            authority = authority.substring(0, index);
        }
        return authority;
    }

    /**
     * @param url the uri to check
     * @param origin the origin to check against.
     * @return true if the given uri is the same origin as the given origin uri otherwise returns false.
     */
    private static boolean check(URI url, URI origin) {
        if (!url.isAbsolute() || !origin.isAbsolute() || url.isOpaque() || origin.isOpaque()) {
            return false;
        }

        return Objects.equals(origin.getScheme(), url.getScheme())
                && getPortForUri(origin) == getPortForUri(url)
                && Objects.equals(normaliseHost(origin), normaliseHost(url));
    }

    /**
     * Returns the port for a given url.
     *
     * @param url the url to get the port of.
     * @return the port for a given url.
     */
    static int getPortForUrl(URL url) {
        int port = url.getPort();
        return port != -1 ? port : url.getDefaultPort();
    }

    /**
     * Returns the port for a given uri, using logic from {@link #getPortForUrl} to get a default port if necessary.
     *
     * @param uri the uri to get the port of.
     * @return the port (possibly a default) for a given uri.
     */
    private static int getPortForUri(URI uri) {
        int uriPort = uri.getPort();
        if (uriPort != -1) {
            return uriPort;
        }

        URL url;
        try {
            url = uri.toURL();
        } catch (MalformedURLException | IllegalArgumentException e) {
            return uriPort;
        }
        return getPortForUrl(url);
    }
}
