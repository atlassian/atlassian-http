package com.atlassian.http.mime;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A tool for loading and encapsulating the local policy for which MIME Content Types and file extensions may contain
 * active executable client-side content and which therefore should be treated carefully to avoid XSS attacks via
 * uploading these files as attachments.
 * <p>
 * Browsers use Content-Type headers and file extensions to decide whether to attempt to execute a file in a client
 * context. Examples include javascript in html and ActionScript in .swf (Flash) binaries. Since these runtimes have
 * access to the client-side state of the browser, they represent a potential means to steal session cookie contents and
 * other XSS attacks.
 * </p>
 */
public class HostileExtensionDetector {
    private static final Logger log = LoggerFactory.getLogger(HostileExtensionDetector.class);

    /**
     * File extensions and content types in the config file are parsed from a list of items delimited by this regex.
     */
    private static final String DELIMITER_REGEX = "\\s+";

    /**
     * Property key for list of executable MIME types.
     */
    private static final String KEY_EXECUTABLE_CONTENT_TYPES = "executable.mime.types";

    /**
     * Property
     */
    private static final String KEY_EXECUTABLE_IF_NO_EXT_NO_MIME = "executable.if.no.extension.no.mime";

    /**
     * Property key for list of executable file extensions.
     */
    private static final String KEY_EXECUTABLE_FILE_EXTENSIONS = "executable.file.extensions";

    /**
     * Property key for the list of file extensions that is considered text file (and should display as plain text in
     * browser)
     */
    private static final String KEY_TEXT_FILE_EXTENSIONS = "text.file.extensions";

    /**
     * Property key for list of content type that is considered text.
     */
    private static final String KEY_TEXT_FILE_CONTENT_TYPES = "text.file.mime.types";

    /**
     * Property key for list of content types considered safe.
     */
    private static final String KEY_SAFE_CONTENT_TYPES = "safe.file.mime.types";

    /**
     * The name of the configuration file.
     */
    private static final String CONFIG_FILE = "hostile-attachments-config.properties";

    /**
     * http://www.flashcomguru.com/index.cfm/2007/11/13/flash-video-mime-types http://www.kaourantin.net/2007/10/new-file-extensions-and-mime-types.html
     */
    private static final Set<String> DEFAULT_EXECUTABLE_FILE_EXTENSIONS = Set.of(
            ".htm",
            ".html",
            ".xhtml",
            ".xml",
            ".shtml", // html related
            ".svg",
            ".swf",
            ".cab",
            ".flv",
            ".f4v",
            ".f4p",
            ".f4a",
            ".f4b" // flash related
            );

    /**
     * A set of extensions that could be considered executable, but is made up of text, and can be displayed safely in
     * all non-ie browsers if the content-type is set to text/plain
     */
    private static final Set<String> DEFAULT_TEXT_FILE_EXTENSIONS = Set.of(".txt");

    /**
     * A set of content type that is considered to be made of text.
     */
    private static final Set<String> DEFAULT_TEXT_FILE_CONTENT_TYPES = Set.of("text/plain");

    /**
     * Match the media type (as defined in RFC2046) up to the parameter list.
     *
     * E.g
     *
     * "text/plain; charset=iso-8859-1" -> group(1) will contain "text/plain"
     *
     * RFC2046
     */
    private static final Pattern VALID_MIME_TYPE =
            Pattern.compile("([a-z0-9_-]+/[^;\\s]+)+.*", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    /**
     * https://www.ietf.org/rfc/rfc3023.txt
     *
     * This must be kept in sync with executable.mime.types in hostile-attachments-config.properties.
     */
    private static final Set<String> DEFAULT_EXECUTABLE_CONTENT_TYPES = Set.of(
            "text/html",
            "text/html-sandboxed",
            "text/xhtml",
            "application/xhtml+xml",
            "text/xml",
            "application/xml",
            "text/xml-external-parsed-entity",
            "application/xml-external-parsed-entity",
            "application/xml-dtd",
            "application/x-shockwave-flash",
            "image/svg+xml",
            "image/svg-xml",
            "application/futuresplash",
            "application/x-cab",
            "video/x-flv",
            "application/octet-stream",
            "application/pdf",
            "message/rfc822",
            "text/vnd.wap.wml",
            "application/atom+xml",
            "text/webviewhtml",
            "application/rdf+xml",
            "application/mathml+xml",
            "multipart/x-mixed-replace",
            "application/vnd.wap.xhtml+xml",
            "text/rdf",
            "text/xsl",
            "text/vtt");

    private Set<String> executableFileExtensions;
    private Set<String> textfileExtensions;
    private Set<String> textfileContentTypes;
    private Set<String> executableContentTypes;
    private Set<String> safeContentTypes;

    /**
     * Loads the configuration of what are executable file extensions and content types from the given properties
     *
     */
    public HostileExtensionDetector() {
        loadConfiguration();
    }

    /**
     * Loads the configuration of what are executable file extensions and content types from the default configuration
     * file, {@link #CONFIG_FILE}.
     *
     * @throws IOException if there is a problem loading from the default configuration file.
     */
    private void loadConfiguration() {
        final Properties config = new Properties();
        final InputStream in = getClass().getResourceAsStream(CONFIG_FILE);
        if (in != null) {
            try {
                config.load(in);
            } catch (IOException e) {
                log.warn("Unable to load config from '" + CONFIG_FILE + "' falling back to defaults ");
            } finally {
                try {
                    in.close();
                } catch (IOException ignored) {
                }
            }
        } else {
            log.warn("Unable to load config from '" + CONFIG_FILE + "' falling back to defaults ");
        }
        parseConfiguration(config);
    }

    /**
     * Parses the lists of executable content types and file extensions from their respective config property values.
     *
     * @param config the Properties that contains the config.
     */
    private void parseConfiguration(final Properties config) {
        executableFileExtensions =
                getProperty(config, KEY_EXECUTABLE_FILE_EXTENSIONS, DEFAULT_EXECUTABLE_FILE_EXTENSIONS);
        executableContentTypes = getProperty(config, KEY_EXECUTABLE_CONTENT_TYPES, DEFAULT_EXECUTABLE_CONTENT_TYPES);

        textfileExtensions = getProperty(config, KEY_TEXT_FILE_EXTENSIONS, DEFAULT_TEXT_FILE_EXTENSIONS);
        textfileContentTypes = getProperty(config, KEY_TEXT_FILE_CONTENT_TYPES, DEFAULT_TEXT_FILE_CONTENT_TYPES);
        safeContentTypes = getProperty(config, KEY_SAFE_CONTENT_TYPES, Collections.emptySet());
    }

    private Set<String> getProperty(final Properties config, final String key, final Set<String> defaultValue) {
        final String extensions = config.getProperty(key);
        if (log.isDebugEnabled()) {
            log.debug("Configured executable file extensions: '" + extensions + "'");
        }
        if (!StringUtils.isBlank(extensions)) {
            return Set.copyOf(
                    Arrays.stream(extensions.toLowerCase(Locale.US).trim().split(DELIMITER_REGEX))
                            .toList());
        }
        return new HashSet<String>(defaultValue);
    }

    /**
     * Determines if the given String has an extension denoting a client-executable active content type such that if the
     * browser opens the file, its execution could have access to the browser DOM etc. Examples include .html, .svg and
     * .swf. Note the check is case insensitive.
     *
     * @param name the file name.
     * @return true only if the name has one of the configured extensions.
     */
    public boolean isExecutableFileExtension(final String name) {
        boolean isExecutableFileExtension = false;
        if (!StringUtils.isBlank(name)) {
            isExecutableFileExtension = executableFileExtensions.contains(getFileExtension(name));
        }
        return isExecutableFileExtension;
    }

    /**
     * @param name a file name. Cannot be null.
     * @return the extension if the file name, if any. Returns an empty string if there is no "." character in the file
     *         name.
     */
    private String getFileExtension(String name) {
        return name.contains(".")
                ? name.substring(name.lastIndexOf("."), name.length()).toLowerCase(Locale.US)
                : "";
    }

    /**
     * Determines if the given String contains a substring of a MIME Content Type denoting client-executable active content such that if the
     * browser opens the file, its execution could have access to the browser DOM etc. E.g. text/html Note the check is
     * case insensitive.
     *
     * @param contentType the MIME Content Type string.
     * @return true only if the given contentType contains a substring of one of the configured executable Content Types
     * or the contentType is empty or is an invalid contentType.
     */
    public boolean isExecutableContentType(final String contentType) {
        boolean isExecutableContentType = false;
        if (StringUtils.isBlank(contentType)) {
            return true;
        }
        /* browsers can sniff the content when a non-parsable Content Type is provided */
        if (!VALID_MIME_TYPE.matcher(contentType.toLowerCase()).matches()) {
            return true;
        }

        for (String executableContentType : executableContentTypes) {
            if (contentType.toLowerCase(Locale.US).contains(executableContentType)) {
                isExecutableContentType = true;
                break;
            }
        }
        return isExecutableContentType;
    }

    public boolean isSafeContentType(final String contentType) {
        return !StringUtils.isBlank(contentType) && safeContentTypes.contains(contentType);
    }

    /**
     * @param fileName can be null or empty.
     * @return true if the extension of the given filename is of type text. False other wise. defaults to false if filename is null or empty.
     */
    public boolean isTextExtension(String fileName) {
        boolean isTextFileExtension = false;
        if (!StringUtils.isBlank(fileName)) {
            isTextFileExtension = textfileExtensions.contains(getFileExtension(fileName));
        }
        return isTextFileExtension;
    }

    /**
     *
     * @param contentType the content type
     * @return true if the content type given is of a text type. False otherwise
     */
    public boolean isTextContentType(String contentType) {
        boolean isTextContentType = false;
        if (!StringUtils.isBlank(contentType)) {
            isTextContentType = textfileContentTypes.contains(contentType.toLowerCase(Locale.US));
        }
        return isTextContentType;
    }

    /**
     * Determines if the file is executable given its file name and content type.
     * Special consideration is given to when the file has no extension and no content type -
     * it is considered executable only if the {@code KEY_EXECUTABLE_IF_NO_EXT_NO_MIME}
     * property was set to true (true by default)
     *
     * @param fileName the name of the file
     * @param contentType the content type
     * @return true if the file is executable given its name and content type
     */
    public boolean isExecutableContent(String fileName, String contentType) {
        return isExecutableFileExtension(fileName) || isExecutableContentType(contentType);
    }

    /**
     * @param fileName the name of the file
     * @param contentType the content type
     * @return true if the file is text given its name and content type
     */
    public boolean isTextContent(String fileName, String contentType) {
        // Note we are conservative here wrt what is considered a text file (&& vs ||)
        return isTextExtension(fileName) && isTextContentType(contentType);
    }

    Set<String> getSafeContentTypes() {
        return safeContentTypes;
    }

    Set<String> getExecutableContentTypes() {
        return executableContentTypes;
    }

    Set<String> getDefaultExecutableContentTypes() {
        return DEFAULT_EXECUTABLE_CONTENT_TYPES;
    }

    Set<String> getExecutableFileExtensions() {
        return executableFileExtensions;
    }
}
