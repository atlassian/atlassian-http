#!/bin/bash

MAVEN_SETTINGS=$MAVEN_HOME/conf/settings.xml
echo "Messing up with Maven settings:"
echo $MAVEN_SETTINGS

sed -i'back' '/<servers>/ a\
<server><id>atlassian-private</id><username>${atlassian_private_username}</username><password>${atlassian_private_password}</password></server>' $MAVEN_SETTINGS

sed -i'back' '/<servers>/ a\
<server><id>atlassian-restricted</id><username>${atlassian_private_username}</username><password>${atlassian_private_password}</password></server>' $MAVEN_SETTINGS

sed -i'bak' '/<profiles>/ a\
<profile><id>atlassian-private</id><activation><activeByDefault>true</activeByDefault></activation><repositories><repository><id>atlassian-private</id><name>Atlassian Private</name><url>https://packages.atlassian.com/maven/private</url><layout>default</layout></repository><repository><id>atlassian-private-snapshot</id><name>Atlassian Private Snapshot</name><url>https://packages.atlassian.com/maven/atlassian-private-snapshot</url><layout>default</layout></repository></repositories><pluginRepositories><pluginRepository><id>atlassian-private</id><url>https://packages.atlassian.com/maven/repository/internal</url></pluginRepository></pluginRepositories></profile>' $MAVEN_SETTINGS

sed -i'bak' '/<profiles>/ a\
<profile><id>atlassian-restricted</id><activation><activeByDefault>true</activeByDefault></activation><repositories><repository><id>atlassian-restricted</id><name>Atlassian Restricted</name><url>https://packages.atlassian.com/maven/restricted</url><layout>default</layout></repository><repository><id>atlassian-restricted-snapshot</id><name>Atlassian Restricted Snapshot</name><url>https://packages.atlassian.com/maven/atlassian-restricted-snapshot</url><layout>default</layout></repository></repositories><pluginRepositories><pluginRepository><id>atlassian-restricted</id><url>https://packages.atlassian.com/maven/repository/internal</url></pluginRepository></pluginRepositories></profile>' $MAVEN_SETTINGS

sed -i'bak' '/<profiles>/ a\
<profile><id>atlassian-public</id><activation><activeByDefault>true</activeByDefault></activation><repositories><repository><id>atlassian-public</id><url>https://packages.atlassian.com/maven/repository/public</url></repository></repositories></profile>' $MAVEN_SETTINGS

